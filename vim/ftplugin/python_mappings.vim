"--------------------------------------------------------------------------------
"     Author              :     Daniel Lupu
"     Creation Date       :     [2015-11-13 00:43]
"     Last Modified       :     [2019-01-31 02:03]
"--------------------------------------------------------------------------------

function! RunNeoformat()
    try
        undojoin | Neoformat
    catch /./
        execute 'Neoformat'
    endtry
endfunction

" save, clear, run with python
map <silent> <Leader>R :call VimuxRunCommand('clear; python ' . bufname("%"))<CR>

" check syntax
map <silent> <Leader>R :call VimuxRunCommand('clear; python -m py_compile ' . bufname("%"))<CR>

" run linters
map <silent> <Leader>L :call VimuxRunCommand('clear; pylint ' . bufname("%") . '; pep8 '. bufname("%"))<CR>

" format code with yapf
map <Leader>F :w <CR>:!clear <CR>:0,$!yapf --style=$HOME/.style.yapf<CR><CR>

