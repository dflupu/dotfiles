"--------------------------------------------------------------------------------
"     Author              :     Daniel Lupu
"     Creation Date       :     [2015-11-13 01:16]
"     Last Modified       :     [2016-05-04 19:46]
"--------------------------------------------------------------------------------

" run current file with node
map <silent> <Leader>R :call VimuxRunCommand('clear; node ' . bufname("%"))<CR>

" run eslint on current file
map <silent> <Leader>L :call VimuxRunCommand('clear; eslint ' . bufname("%"))<CR>
