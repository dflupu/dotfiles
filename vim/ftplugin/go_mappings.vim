"--------------------------------------------------------------------------------
"     Author              :     Daniel Lupu
"     Creation Date       :     [2015-11-13 00:15]
"     Last Modified       :     [2015-11-28 00:45]
"--------------------------------------------------------------------------------
noremap <Leader>R :GoRun<CR>
noremap <Leader>T :GoTest<CR>
noremap <Leader>rr :GoRename<CR>
noremap K :GoDoc<CR>

set ts=8 sw=8
