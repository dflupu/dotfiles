let SSHHOME = expand('$SSHHOME/.sshrc.d/.vim/')
exe 'set rtp+=' . SSHHOME

source $SSHHOME/.sshrc.d/.vimconfig/options
source $SSHHOME/.sshrc.d/.vimconfig/functions
source $SSHHOME/.sshrc.d/.vimconfig/binds
source $SSHHOME/.sshrc.d/.vimconfig/auto
